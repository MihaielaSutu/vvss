package biblioteca.control.test;


;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class Test2 {

    public static CartiRepo cartiRepo;

    @BeforeClass
    public static void instantiate() {
        cartiRepo = new CartiRepo();
    }

    @Test
    public void testValidSearchBooksByAuthor1() {
        try {
            cartiRepo.cautaCarte("caragiale");
            assertTrue(true);
        } catch (Exception e) {
        }
    }

    @Test
    public void testInvalidSearchBooksByAuthor1() {
        try {
            cartiRepo.cautaCarte("ion carag2iale");
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void testValidSearchBooksByAuthor2() {
        try {
            List<Carte> rez = cartiRepo.cautaCarte("caragiale");
            assertEquals(1, rez.size());
        } catch (Exception e) {
        }
    }

    @Test
    public void testValidSearchBooksByAuthor3() {
        try {
            List<Carte> rez = cartiRepo.cautaCarte("Vasilica");
            assertEquals(0, rez.size());
        } catch (Exception e) {
        }
    }

    @Test
    public void testInvalidSearchBooksByAuthor2() {
        try {
            List<Carte> rez = cartiRepo.cautaCarte("");
        } catch (Exception e) {
            assertTrue(true);
        }
    }
}

