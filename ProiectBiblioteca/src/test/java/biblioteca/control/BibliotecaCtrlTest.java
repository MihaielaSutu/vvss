package biblioteca.control;

import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import static org.junit.Assert.*;

public class BibliotecaCtrlTest {

    @Test
    public void testAdaugaCarteAutorValid() throws Exception
    {
        Carte carte = new Carte();
        carte.setAnAparitie("2010");
        carte.setTitlu("Jurnalul fericirii");
        carte.setCuvinteCheie(Arrays.asList("jurnal"));
        carte.setReferenti(Arrays.asList("Nicolae Steihardt"));
        CartiRepoInterface cartiRepo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(cartiRepo);

        int size = ctrl.getCarti().size();
        ctrl.adaugaCarte(carte);

        assertEquals(size + 1, ctrl.getCarti().size());
    }

    @Test(expected = Exception.class)
    public void testAdaugaCarteTitluInvalid() throws Exception
    {
        Carte carte = new Carte();
        carte.setAnAparitie("1995");
        carte.setTitlu("Book23");
        carte.setCuvinteCheie(Arrays.asList("jurnal"));
        carte.setReferenti(Arrays.asList("Nicolae Steihardt"));
        CartiRepoInterface cartiRepo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(cartiRepo);

        ctrl.adaugaCarte(carte);
    }

    @Test(expected = Exception.class)
    public void testAdaugaCarteAutorInvalid() throws Exception
    {
        Carte carte = new Carte();
        carte.setAnAparitie("2010");
        carte.setTitlu("Book title");
        carte.setCuvinteCheie(Arrays.asList("nuvela"));
        carte.setReferenti(Arrays.asList("123"));
        CartiRepoInterface cartiRepo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(cartiRepo);

        ctrl.adaugaCarte(carte);
    }

    @Test(expected = Exception.class)
    public void testAdaugaCarteAnAparitieInvalid() throws Exception
    {
        Carte carte = new Carte();
        carte.setAnAparitie("an aparitie");
        carte.setTitlu("Book title");
        carte.setCuvinteCheie(Arrays.asList("roman"));
        carte.setReferenti(Arrays.asList("Autor"));
        CartiRepoInterface cartiRepo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(cartiRepo);

        ctrl.adaugaCarte(carte);
    }

    @Test(expected = Exception.class)
    public void testAdaugaCarteInvalida() throws Exception
    {
        Carte carte = new Carte();
        carte.setAnAparitie("1a2b");
        carte.setTitlu("123");
        carte.setCuvinteCheie(Arrays.asList("roman"));
        carte.setReferenti(Arrays.asList("Autor123"));
        CartiRepoInterface cartiRepo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(cartiRepo);

        ctrl.adaugaCarte(carte);
    }

    @Test
    public void testAdaugaCarteTitluLungime1() throws Exception
    {
        Carte carte = new Carte();
        carte.setAnAparitie("1990");
        carte.setTitlu("A");
        carte.setCuvinteCheie(Arrays.asList("roman"));
        carte.setReferenti(Arrays.asList("Ion Creanga","Mihai Eminescu"));
        CartiRepoInterface cartiRepo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(cartiRepo);

        int size = ctrl.getCarti().size();
        ctrl.adaugaCarte(carte);

        assertEquals(size + 1, ctrl.getCarti().size());
    }

    @Test
    public void testAdaugaCarteTitluLungime255() throws Exception
    {
        Carte carte = new Carte();
        carte.setAnAparitie("1900");
        carte.setTitlu(generateTitle(255));
        carte.setCuvinteCheie(Arrays.asList("roman"));
        carte.setReferenti(Arrays.asList("Ion Creanga","Mihai Eminescu"));
        CartiRepoInterface cartiRepo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(cartiRepo);

        int size = ctrl.getCarti().size();
        ctrl.adaugaCarte(carte);

        assertEquals(size + 1, ctrl.getCarti().size());
    }

    @Test
    public void testAdaugaCarteTitluLungime254() throws Exception
    {
        Carte carte = new Carte();
        carte.setAnAparitie("1787");
        carte.setTitlu(generateTitle(254));
        carte.setCuvinteCheie(Arrays.asList("roman"));
        carte.setReferenti(Arrays.asList("Ion Creanga","Mihai Eminescu"));
        CartiRepoInterface cartiRepo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(cartiRepo);

        int size = ctrl.getCarti().size();
        ctrl.adaugaCarte(carte);

        assertEquals(size + 1, ctrl.getCarti().size());
    }

    private String generateTitle(int size) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 10;
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(size);
        for (int i = 0; i < size; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    @Test
    public void testAdaugaCarteAn0() throws Exception
    {
        Carte carte = new Carte();
        carte.setAnAparitie("0");
        carte.setTitlu("Titlu");
        carte.setCuvinteCheie(Arrays.asList("roman"));
        carte.setReferenti(Arrays.asList("Ion Creanga","Mihai Eminescu"));
        CartiRepoInterface cartiRepo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(cartiRepo);

        int size = ctrl.getCarti().size();
        ctrl.adaugaCarte(carte);

        assertEquals(size + 1, ctrl.getCarti().size());
    }

    @Test(expected = Exception.class)
    public void testAdaugaCarteAnMinus1() throws Exception
    {
        Carte carte = new Carte();
        carte.setAnAparitie("-1");
        carte.setTitlu("Titlu");
        carte.setCuvinteCheie(Arrays.asList("roman"));
        carte.setReferenti(Arrays.asList("Ion Creanga","Mihai Eminescu"));
        CartiRepoInterface cartiRepo = new CartiRepo();
        BibliotecaCtrl ctrl = new BibliotecaCtrl(cartiRepo);

        ctrl.adaugaCarte(carte);
    }

}